"""
Prog:   ex-08.py

Auth:   R. Morgan

Desc:   Demonstrate how to use John Zelle's
        graphics.py library for drawing
        rectangles to the window.

Lic:    This code is placed in the public domain.

"""

from graphics import *
from random import randint

def main():
    win = GraphWin("Rectangles", 640, 480)
    
    for i in range(1000):
        p1 = Point(randint(0,639), randint(0,479))
        p2 = Point(randint(0,639), randint(0,479))

        r = randint(0,255)
        g = randint(0,255)
        b = randint(0,255)

        rect = Rectangle(p1,p2)
        rect.setOutline(color_rgb(r, g, b))
        rect.draw(win)

    win.getMouse()
    win.close()

main()