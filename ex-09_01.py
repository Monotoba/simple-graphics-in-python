#!/usr/bin/env python3

"""
Prog:   ex-09_01.py

Auth:   R. Morgan

Desc:   Demonstrate how to use John Zelle's
        graphics.py library to draw ovals 
        using points.

Lic:    This code is placed in the public domain.

"""

from graphics import *
from math import *
from random import randint

# Given the center x, center y
# and radius, draw a circle 
# using points.
def oval(cx, cy, rx, ry, color, win):
    
    for i in range(0, 360):
        x = cos(i)*rx + cx;
        y = sin(i)*ry + cy

        p = Point(x,y)
        p.setFill(color)
        p.draw(win)


def main():
    win = GraphWin("Circles", 400, 400)
    win.setBackground(color_rgb(0,0,0))

    for i in range(0, 20):
        rx = randint(10, 200)
        ry = randint(10, 200)

        r = randint(0,255)
        g = randint(0,255)
        b = randint(0,255) 
        oval(200, 200, rx, ry, color_rgb(r, g, b), win)
   
    win.getMouse()
    win.close()


main()
