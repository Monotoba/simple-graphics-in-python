"""
Prog:   ex-08_01.py

Auth:   R. Morgan

Desc:   Demonstrate how to use John Zelle's
        graphics.py library for drawing
        rectangles to the window.

Lic:    This code is placed in the public domain.

"""

from graphics import *
from random import randint

def draw_rect(x, y, w, h, win):
    # Build points for the rect's corners
    p1 = Point(x,y)
    p2 = Point(x+w, y)
    p3 = Point(x+w, y+h)
    p4 = Point(x, y+h)
    points = (p1, p2, p3, p4)

    # draw the lines to connect the points
    lines = []
    lines.append(Line(p1, p2))
    lines.append(Line(p2, p3))
    lines.append(Line(p3, p4))
    lines.append(Line(p4, p1))

    # set the line color
    # and draw
    for ln in lines:
        r = randint(0, 255)
        g = randint(0, 255)
        b = randint(0, 255)
        ln.setFill(color_rgb(r, g, b))
        ln.draw(win)
    
    
    
def main():
    win = GraphWin("Rectangles", 400, 400)

    for i in range(0,50):
        x = randint(0, 400)
        y = randint(0, 400)
        w = randint(0,400)
        h = randint(0,400)
        draw_rect(x, y, w, h, win)

    win.getMouse()
    win.close()

main()