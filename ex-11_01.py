"""
Prog:   ex-11_01.py

Auth:   R. Morgan

Desc:   Demonstrate how to use John Zelle's
        graphics.py library for images into
        the window.

Lic:    This code is placed in the public domain.

"""

from graphics import *

def main():
    win = GraphWin("Image Loader", 640, 480)
    win.setBackground(color_rgb(0,0,0))

    p1 = Point(320, 240) # Point of center of image
    img = Image(p1, 'images/PixelCar.gif')
    img.draw(win)
    win.getMouse()
    win.close()

main() # Call main()
